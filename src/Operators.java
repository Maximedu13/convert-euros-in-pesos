import java.math.BigDecimal;
import java.util.Scanner;
import java.util.*;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Operators {
    private static DecimalFormat df = new DecimalFormat("0.00");
    public static void main(String[] args){
        Double rate = 913.65;
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir votre argent en euros :");
        Integer str = sc.nextInt();
        double result = str * rate;
        System.out.println(str + " euros valent " + df.format(result) + " pesos chiliens.");
        }

}
